window.HAM = (function(HAM, $, window, document) {
    "use strict";

    HAM.Utils = {
        pushUnique: function(array, item) {
            if (array.indexOf(item) === -1) {
                array.push(item);
            }
        },

        getURLParameter: function(name) {
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
        },

        substringMatcher: function(strs) {
            return function findMatches(q, cb) {
                var matches, substrRegex;
                matches = [];
                substrRegex = new RegExp(q, 'i');
                $.each(strs, function(i, str) {
                    if (substrRegex.test(str)) {
                        matches.push(str);
                    }
                });
                cb(matches);
            };
        }
    };
    return HAM;
}(window.HAM || {}, window.jQuery, window, document));