window.HAM = (function(HAM, $, window, document) {
    "use strict";
    var c;
    var hash;
    var asset;
    HAM.Edit = {
        config: {
            name: '.js-edit-name',
            img: '.js-edit-img',
            tags: '.js-tags',
            addTag: '.js-edit-tag-add',
            removeTag: '.js-edit-tag-remove',
            input: '.js-edit-input',
        },

        init: function() {
            c = this.config;
            hash = HAM.Utils.getURLParameter('id');
            asset = HAM.Gallery.getAsset(hash);
            this.bindEvents();
            this.populateEditForm(asset);
        },

        bindEvents: function() {
            $(c.addTag).on('click', this.handleAddTag.bind(this));
            $(document).on('click', c.removeTag, this.handleRemoveTag.bind(this));
            $(c.input).on('keypress', this.handleInputKeypress.bind(this));
        },

        renderTag: function(tag) {
            var deleteAction = tag === "--none" ? "" : "<a href='#' class='text-danger js-edit-tag-remove'><i class='fa fa-times'></i></a>";
            return "<span class='tag tag-lg tag-warning' id='" + tag + "'>" + tag + " " + deleteAction + " </span>";
        },

        populateEditForm: function(asset) {
            $(c.name).html(asset.name);
            $(c.img).hide().attr('src', asset.thumb).fadeIn();
            var tags = "";
            var self = this;
            asset.tags.forEach(function(tag) {
                tags += self.renderTag(tag);
            });

            $(c.tags).html(tags);
        },

        handleInputKeypress: function(e) {
            if (e.which == 13) {
                this.handleAddTag(e);
            }
        },

        handleAddTag: function(e) {
            var tag = $(c.input).val();
            var self = this;
            this.showOverlay();
            this.setMessage('');
            this.requestAddTag(tag).done(function() {
                $(c.input).val('');
                if ($(c.tags).text().trim() === "--none") {
                    $(c.tags).html('');
                }
                $(c.tags).append(self.renderTag(tag));
                self.hideOverlay();
            }).fail(function(e) {
                $(c.input).val('');
                var error = JSON.parse(e.responseText);
                self.setMessage(error.msg);
                self.hideOverlay();
            });
        },

        handleRemoveTag: function(e) {
            var tag = $(e.currentTarget).parent('span').attr('id');
            var self = this;
            self.e = e;
            this.showOverlay();
            this.requestRemoveTag(tag).done(function() {
                $(self.e.target).parent('span').remove();
                $('#' + tag).remove();
                if ($(c.tags).children().length == 0) {
                    $(c.tags).append("<span class='tag tag-lg tag-warning' id='--none'> --none </span>")
                }
                self.hideOverlay();
            }).fail(function(e) {
                self.hideOverlay();
            });
        },

        requestAddTag: function(tag) {
            return $.ajax({
                url: '../resources/actions/assets/addTag.php',
                method: 'POST',
                data: {
                    hash: hash,
                    tag: tag
                },
            });
        },

        requestRemoveTag: function(tag) {
            return $.ajax({
                url: '../resources/actions/assets/removeTag.php',
                method: 'POST',
                data: {
                    hash: hash,
                    tag: tag
                },
            });
        },

        showOverlay: function() {
            $('.js-overlay').show();
        },

        hideOverlay: function() {
            $('.js-overlay').hide();
        },

        setMessage: function(text) {
            $('.js-message').html(text);
        },

        // editAssetHandler: function(data) {
        //     var $message = $('#edit-asset__message');
        //     if (data['status'] === 'success') {
        //         window.location.href = "/index.html";
        //     } else {
        //         $message.removeClass().addClass('error');
        //         $message.html('An error has occurred. Please check the form and try again');
        //     }
        // }
    };
    HAM.Edit.init();
    return HAM;
}(window.HAM || {}, window.jQuery, window, document));