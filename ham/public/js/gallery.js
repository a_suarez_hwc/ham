window.HAM = (function(HAM, window, document, $) {
    'use strict';
    var c;
    var tags = [];
    var assets = [];

    HAM.Gallery = {
        config: {
            el: '#assets-container',
            item: '.asset',
            overlay: '.js-overlay',
            refresh: '.js-gallery-refresh',
            clipboardInput: '.js-clip-input',
            clipboardBtn: '.js-clip-btn',
            download: '.js-download',
            modal: '.js-gallery-modal',
            title: '.js-gallery-modal-title',
            edit: '.js-gallery-modal-edit',
            img: '.js-gallery-modal-img',
            tooltip: '[data-toggle="tooltip"]',
        },

        init: function() {
            c = this.config;
            c.$el = $(c.el);
            var clipboard = new Clipboard(c.clipboardBtn);
            $(c.tooltip).tooltip()

            this.bindEvents();
            this.render();
        },

        bindEvents: function() {
            $(document).on('click', c.item, this.handleItemClick.bind(this));
            $(c.refresh).on('click', this.handleRefresh.bind(this));
            $(c.clipboardBtn).on('click', this.callPreventDefault.bind(this));
            $(c.modal).on('hide.bs.modal', this.hideTooltip.bind(this));
        },

        render: function() {
            this.requestAssets()
                .success(this.appendGalleryItems.bind(this))
                .done(this.completeRender.bind(this));
        },

        appendGalleryItems: function(items) {
            var galleryContent = "";
            assets = items;
            items.forEach(function(item) {
                var tagsContent = "";
                item.tags.forEach(function(tag) {
                    HAM.Utils.pushUnique(tags, tag);
                    tagsContent += "<span class='tag tag-lg tag-warning'>" + tag + "</span>";
                });
                galleryContent += "<div class='asset' id=" + item.hash + " data-hash=" + item.hash + ">" +
                    "<img class='asset__preview img-fluid lazy' data-src=" + item.thumb + " />" +
                    "<span class='asset__overlay'><div class='asset__tags'>" + tagsContent + "</div></span>" +
                    "<i class='fa fa-info-circle fa-2x asset__info'></i></div>";
            });
            tags.sort();
            c.$el.append(galleryContent);
        },

        completeRender: function() {
            $(".lazy").lazy({
                effect: "fadeIn",
                effectTime: 250,
                bind: 'event'
            });
            HAM.Filter.init();
        },

        handleRefresh: function(e) {
            $(c.overlay).show();
            this.requestRefresh().done(function(e) {
                if (window.location.href.indexOf("index.html") !== -1) {
                    location.reload();
                } else {
                    window.location.href = "index.html";
                }
            });
        },

        handleItemClick: function(e) {
            var item = this.getAsset(e.currentTarget.dataset.hash);
            $(c.title).html(item.name);
            $(c.download).attr('href', item.download);
            $(c.download).attr('download', item.name);
            $(c.edit).attr('href', 'edit.html?id=' + item.hash);
            $(c.img).attr('src', item.thumb);
            $(c.modal).modal();
        },

        getTags: function() {
            return tags;
        },

        getAsset: function(hash) {
            return assets.filter(function(item) {
                return hash === item.hash
            })[0];
        },

        getAssets: function() {
            return assets;
        },

        requestAssets: function() {
            return $.ajax({
                url: 'data.json',
                dataType: 'json',
                async: false,
            });
        },

        requestAsset: function(hash) {
            return $.ajax({
                url: '../resources/actions/assets/show.php',
                dataType: 'json',
                data: {
                    id: hash
                },
            });
        },

        requestRefresh: function() {
            return $.ajax({
                url: '../resources/actions/refresh.php',
                method: 'GET'
            });
        },

        callPreventDefault: function(e) {
            e.preventDefault();
        },

        hideTooltip: function(e) {
            $(c.tooltip).tooltip('hide');
        }
    };
    HAM.Gallery.init();
    return HAM;
})(window.HAM || {}, window, document, jQuery);