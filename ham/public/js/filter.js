window.HAM = (function(HAM, $, window, document) {
    "use strict";
    var c;
    HAM.Filter = {
        config: {
            el: '.js-tt',
            item: '.asset',
            input: '.tt-input',
            filterBtn: '.js-filter',
            resetBtn: '.js-reset'
        },

        init: function() {
            c = this.config;
            var tags = HAM.Gallery.getTags();
            this.bindEvents();

            $(c.el).typeahead({
                hint: true,
                highlight: true,
                minLength: 0,
                items: 9999,
            }, {
                name: 'tags',
                limit: tags.length,
                source: HAM.Utils.substringMatcher(tags)
            });
        },

        bindEvents: function() {
            $(c.filterBtn).on('click', this.filter.bind(this));
            $(c.resetBtn).on('click', this.reset.bind(this));
        },

        filter: function(e) {
            e.preventDefault();
            var filter = $(c.input).typeahead('val');

            if (!filter.length) {
                $(c.item).show();
                return;
            }

            // only show assets with filtered tag
            var assets = HAM.Gallery.getAssets();
            $(c.item).hide();
            assets.filter(function(asset) {
                return asset.tags.indexOf(filter) !== -1;
            }).forEach(function(asset) {
                $('#' + asset.hash).show();
            });
        },

        reset: function(e) {
            e.preventDefault();
            $(c.input).typeahead('val', '');
            $(c.item).show();
        }
    };
    return HAM;
}(window.HAM || {}, window.jQuery, window, document));