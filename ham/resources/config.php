<?php

define("ROOT_DIR", rtrim($_SERVER['DOCUMENT_ROOT'], '/') . "/ham/");
define("PUBLIC_DIR", ROOT_DIR . "public/");
define("RESOURCES_DIR", ROOT_DIR . "resources/");
define("THUMB_DIR", PUBLIC_DIR . "img/thumbs/");
define("DATA_SRC", PUBLIC_DIR . "data.json");
define("ASSET_DIR", PUBLIC_DIR . "assets/");

function update_json_source($data)
{
    $fp = fopen(DATA_SRC, 'w');
    fwrite($fp, json_encode($data));
    fclose($fp);
}

function replace_filename($url, $replacement, $removeTags)
{
    $path_parts = pathinfo($url);
    if ($removeTags) {
        $fn = preg_replace("/\[([^\]]+)\]/", '', $path_parts['filename']);
    } else {
        $fn = $path_parts['filename'];
    }
    return str_replace('file:///', '', $path_parts['dirname'] . '/' . $fn . $replacement . '.' . $path_parts['extension']);
}
