<?php
require_once "config.php";

class Asset
{
    public $path;
    public $basename;
    public $name;
    public $thumb;
    public $hash;
    public $tags = [];

    private $tag_exceptions = ['Z:', '05_Aaron'];

    public function __construct($path)
    {
        $this->path     = $path;
        $this->basename = pathinfo($this->path)['basename'];
        $this->name     = preg_replace("/\[([^\]]+)\]/", '', trim($this->basename . PHP_EOL));
        $this->generateThumbnail();
        $this->generateTags();
        $this->generateHash();
    }

    private function generateHash()
    {
        $this->hash = hash_file('md5', mb_convert_encoding($this->path, 'SJIS'));
    }

    private function generateTags()
    {
        // Add path tags
        // $path_components = explode('/', $this->path);
        // foreach ($path_components as $component) {
        //     if (!in_array($component, $this->tag_exceptions) && $component != $this->basename) {
        //         $this->addTag($component);
        //     }
        // }

        // Add filename tags
        preg_match_all("/\[([^\]]+)\]/", $this->basename, $file_tags);
        $file_tags = !empty($file_tags[1]) ? explode(' ', $file_tags[1][0]) : [];
        foreach ($file_tags as $tag) {
            $this->addTag($tag);
        }

        if (empty($this->tags)) {
            $this->addTag('--none');
        }
    }

    public function addTag($tag)
    {
        if ($tag && !is_null($tag) && !in_array($tag, $this->tags)) {
            array_push($this->tags, $tag);
        }
    }

    public function removeTag($tag)
    {
        $key = array_search($tag, $this->tags);
        if (!is_null($key)) {
            array_splice($this->tags, $key, 1);
        }
    }

    private function generateThumbnail($thumbnail_size = 250)
    {
        $file_path = mb_convert_encoding($this->path, 'SJIS');
        $filename  = preg_replace("/\[([^\]]+)\]/", '', trim(basename($file_path) . PHP_EOL));
        $url       = '../../' . substr(THUMB_DIR . urlencode($filename), strlen(ROOT_DIR));

        if (file_exists($url) && getimagesize($url)[0] == $thumbnail_size) {
            $this->thumb = substr($url, 3);
        }

        list($width, $height, $original_type) = getimagesize($file_path);
        if ($width > $height) {
            $x       = ($width - $height) / 2;
            $y       = 0;
            $min_dim = $height;
        } else {
            $x       = 0;
            $y       = ($height - $width) / 2;
            $min_dim = $width;
        }

        if ($original_type === 2) {
            $im = imagecreatefromjpeg($file_path);
        } else if ($original_type === 1) {
            $im = imagecreatefromgif($file_path);
        } else if ($original_type === 3) {
            $im = imagecreatefrompng($file_path);
        }

        // Failure case
        if (!$im) {
            $im  = imagecreatetruecolor($thumbnail_size, $thumbnail_size);
            $bgc = imagecolorallocate($im, 200, 200, 200);
            $tc  = imagecolorallocate($im, 0, 0, 0);

            imagefilledrectangle($im, 0, 0, $thumbnail_size, $thumbnail_size, $bgc);

            $error_text = "Error: " . $filename . " may be corrupted";
            imagestring($im, 2, 5, $thumbnail_size / 2, $error_text, $tc);
        }

        $ox = imagesx($im);
        $oy = imagesy($im);

        $nx = $thumbnail_size;
        $ny = floor($oy * ($thumbnail_size / $ox));

        $nm = imagecreatetruecolor($thumbnail_size, $thumbnail_size);
        imagecopyresized($nm, $im, 0, 0, $x, $y, $thumbnail_size, $thumbnail_size, $min_dim, $min_dim);
        imagedestroy($im);

        if (!file_exists(THUMB_DIR)) {
            if (!mkdir(THUMB_DIR)) {
                die("There was a problem. Please try again!");
            }
        }

        imagejpeg($nm, $url);
        chmod($url, 0777);
        imagedestroy($nm);
        $this->thumb = substr($url, 3);;
    }
}
