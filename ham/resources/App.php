<?php
require 'config.php';
require_once 'Asset.php';

mb_language("japanese");
header('content-type: text/html; charset=utf-8');
ini_set('memory_limit', '256M');
ini_set('max_execution_time', 0);

class App
{
    public $assets = [];

    public function __construct()
    {

    }

    public function deleteThumbs()
    {
        if (THUMB_DIR != '') {
            $files = glob(THUMB_DIR . '/*');
            foreach ($files as $file) {
                if (is_file($file)) {
                    unlink($file);
                }

            }
        }
        $assets = json_decode(file_get_contents(DATA_SRC), true);
        //TODO: set all thumbnail urls in assets.json to empty string
        $fp = fopen(DATA_SRC, 'w');
        fwrite($fp, json_encode($assets));
        fclose($fp);
    }

    /**
     * Upload assets.json to reflect new file information
     */
    public function refreshData()
    {
        $assets = json_decode(file_get_contents(DATA_SRC), true);
        $paths  = $this->getImagePaths();

        // save image assets to database
        foreach ($paths as $path) {
            $asset = new Asset($path);
            $key   = array_search($asset->name, array_column($assets, 'name'));
            $url   = 'file:///' . str_replace(' ', '%20', $asset->path);

            if ($key !== false) {
                // if file already exists in data.json
                echo $asset->name . ' is already in the system as ' . $assets[$key]['name'] . '<br>';
                $assets[$key]['hash']     = $asset->hash;
                $assets[$key]['url']      = $url;
                $assets[$key]['thumb']    = implode('/', array_map('urlencode', explode('/', $asset->thumb)));
                $assets[$key]['name']     = $asset->name;
                $assets[$key]['tags']     = $asset->tags;
                $assets[$key]['path']     = $asset->path;
                $assets[$key]['download'] = substr($asset->path, strlen(PUBLIC_DIR));
            } else {
                // if file not in data.json
                array_push($assets, array(
                    'hash'     => $asset->hash,
                    'url'      => $url,
                    'thumb'    => implode('/', array_map('urlencode', explode('/', $asset->thumb))),
                    'name'     => $asset->name,
                    'tags'     => $asset->tags,
                    'path'     => $asset->path,
                    'download' => substr($asset->path, strlen(PUBLIC_DIR)),
                ));
            }
            echo "<hr>";
        }

        $fp = fopen(DATA_SRC, 'w');
        fwrite($fp, json_encode($assets));
        fclose($fp);
    }

    public function resetData()
    {
        $this->deleteThumbs();
        $fp = fopen(DATA_SRC, 'w');
        fwrite($fp, '[]');
        fclose($fp);
        $this->refreshData();
    }

    public function getImagePaths()
    {
        // get all image files with a .png and .jpg extensions
        $paths = $this->glob_recursive(ASSET_DIR . "{*.png,*.jpg,*.jpeg,*.JPG,*.PNG}", GLOB_BRACE);

        // convert file urls to proper character encoding
        array_walk_recursive($paths, 'App::convertUTF');
        return $paths;
    }

    /**
     * Update encoding to SJIS
     */
    public function convertUTF(&$value, $key)
    {
        $value = mb_convert_encoding($value, "UTF-8", "SJIS");
    }

    /**
     * Search a directory and it's children for file types
     * @param  String  $pattern
     * @param  integer $flags
     * @return array
     */
    public function glob_recursive($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags);
        foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
            $files = array_merge($files, $this->glob_recursive($dir . '/' . basename($pattern), $flags));
        }
        return $files;
    }
}
