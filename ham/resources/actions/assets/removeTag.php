<?php
require '../../config.php';
$out = [];
// header('Content-Type: application/json; charset=UTF-8');
try {
    if (isset($_POST['hash']) && isset($_POST['tag']) && !empty($_POST['tag'])) {
        $hash   = $_POST['hash'];
        $tag    = $_POST['tag'];
        $assets = json_decode(file_get_contents(DATA_SRC), true);
        $key    = array_search($hash, array_column($assets, 'hash'));
        if ($key !== false) {
            $currentTags = $assets[$key]['tags'];
            if (!in_array($tag, $currentTags)) {
                echo json_encode($assets[$key]);
                return;
            }

            $currentTags = array_values(array_diff($currentTags, array($tag)));
            $tagsStr     = '[' . trim(implode(' ', $currentTags)) . ']';
            if ($tagsStr == "[]") {
                $tagsStr     = "";
                $currentTags = ["--none"];
            }

            $assets[$key]['tags'] = $currentTags;
            $newFilename          = replace_filename($assets[$key]['path'], $tagsStr, true);
            rename(mb_convert_encoding($assets[$key]['path'], "SJIS", 'UTF-8'), mb_convert_encoding($newFilename, "SJIS", 'UTF-8'));
            $assets[$key]['url']      = 'file:///' . $newFilename;
            $assets[$key]['path']     = $newFilename;
            $assets[$key]['download'] = substr($newFilename, strlen(PUBLIC_DIR));

            update_json_source($assets);
            echo json_encode($out, JSON_UNESCAPED_UNICODE);
        }
    } else {
        throw new Exception("Error Processing Request", 1);
    }
} catch (Exception $e) {
    header('HTTP/1.1 400 Bad Request');
    echo json_encode(array(
        'error' => array(
            'msg'  => $e->getMessage(),
            'code' => $e->getCode(),
        ),
    ));
}
