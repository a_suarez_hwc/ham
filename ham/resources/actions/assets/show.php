<?php
require "../../config.php";

if (isset($_GET['id'])) {
    $hash   = $_GET['id'];
    $assets = json_decode(file_get_contents(DATA_SRC), true);
    $key    = array_search($hash, array_column($assets, 'hash'));
    if ($key !== false) {
        echo json_encode($assets[$key]);
    }
}
